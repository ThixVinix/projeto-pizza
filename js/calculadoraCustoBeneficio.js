var btIncluir = document.getElementById("idBtIncluir")
var pizzasCadastradas = [];
var vetorPrecoPorCm2 = [];

// **********************************************************************

btIncluir.addEventListener("click", function (event) {
    event.preventDefault();

    var formulario = document.querySelector("#formPizza");

    var pizza = obterPizza(formulario);

    var isValid = validarFormularioPizza(pizza);

    if (!isValid) {
        return;
    }

    var area = calcularAreaCircular(pizza);
    console.log("Área do círculo calculada: " + area);

    var precoPorCm2 = calcularPrecoPorCm2(pizza, area);
    console.log("Preço por cm² calculado: " + precoPorCm2);

    pizza.precoPorCm2 = precoPorCm2;

    //vetorPrecoPorCm2.push(pizza.precoPizza / pizza.precoPorCm2);

    pizza.diferencaPorcentagem = (pizza.precoPizza / pizza.precoPorCm2).toFixed(2);
    
    adicionarVetorPizzas(pizza);

    pizzasCadastradas.sort(function(x, y) {
        return x.diferencaPorcentagem - y.diferencaPorcentagem
    })

    console.table(pizzasCadastradas)

    //Criar nova linha para a tabela 
    var pizzaTr = montaTr(pizza);


    //Adicionar a nova linha na tabela
    var tBody = document.getElementById("tboIdLinha");
    tBody.appendChild(pizzaTr);

    //Resetar formulario
    formulario.reset();

})

// **********************************************************************

//Extraindo dados do form
function obterPizza(formulario) {
    let pizza = {
        nomePizza: formulario.nmNomePizza.value,
        tamanhoPizza: formulario.nmTamanhoPizza.value,
        precoPizza: formulario.nmPrecoPizza.value,
        precoPorCm2: "------",
        diferencaPorcentagem: "------"
    }
    return pizza
}

//Validando todo o formulário
function validarFormularioPizza(pizza) {
    if (!validarFormVazio(pizza)) {
        alert("Preencha corretamente todos os campos do formulário!");
        return false;
    }

    if (!validarFormNegativo(pizza)) {
        alert("Por favor, insira números positivos maiores do que \"0\".");
        return false;
    }

    if (!validarFormTamanhoDuplicado(pizza)) {
        alert("Esse tamanho de pizza já existe no relatório. Por favor, insira tamanhos diferentes.");
        return false;
    }

    return true;
}

//Validando form nulo/vazio
function validarFormVazio(pizza) {
    if (pizza.nomePizza == null || pizza.tamanhoPizza == null || pizza.precoPizza == null) {
        return false;
    }

    if (pizza.nomePizza.trim() == "" || pizza.tamanhoPizza.trim() == "" || pizza.precoPizza.trim() == "") {
        return false;
    }

    return true;
}

//Validando form negativo
function validarFormNegativo(pizza) {
    if (pizza.tamanhoPizza <= 0 || pizza.precoPizza <= 0) {
        return false;
    }

    return true;
}

//Validando form para tamanhos de pizza duplicados
function validarFormTamanhoDuplicado(pizza) {
    if (pizzasCadastradas != null && pizzasCadastradas.length > 0) {
        for (let index = 0; index < pizzasCadastradas.length; index++) {
            if (pizza.tamanhoPizza == pizzasCadastradas[index].tamanhoPizza) {
                return false;
            }
        }
    }
    return true;
}

//Calculando area da pizza
function calcularAreaCircular(pizza) {
    const pi = 3.14159265359;
    var a, r;

    r = (pizza.tamanhoPizza / 2);

    a = (pi * Math.pow(r, 2));

    return a.toFixed(2);
}

//Calculando preço da pizza por centímetro quadrado
function calcularPrecoPorCm2(pizza, area) {
    var x = (pizza.precoPizza / area);
    return x.toFixed(2);
}

//Adicionando pizza ao vetor
function adicionarVetorPizzas(pizza) {
    pizzasCadastradas.push(pizza);
    console.log(pizzasCadastradas)
}

//Cria UMA linha (um tr)
function montaTr(pizza) {
    const cm = "cm"
    let usuarioTr = document.createElement("tr")
    usuarioTr.classList.add("trClPizzaData")

    var precoPizzaFormat = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(pizza.precoPizza);
    var precoPorCm2Format = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(pizza.precoPorCm2);
    var diferencaPorcentagemFormat = new Intl.NumberFormat('pt-BR', {minimumFractionDigits: 3}).format(pizza.diferencaPorcentagem);

    usuarioTr.appendChild(montaTd(pizza.nomePizza, "tdClInfoNomePizza"))
    usuarioTr.appendChild(montaTd(pizza.tamanhoPizza + cm, "tdClInfoTamanhoPizza"))
    usuarioTr.appendChild(montaTd(precoPizzaFormat, "tdClInfoPrecoPizza"))
    usuarioTr.appendChild(montaTd(precoPorCm2Format, "tdClInfoPrecoPorCm2"))
    usuarioTr.appendChild(montaTd(diferencaPorcentagemFormat + "%", "tdClInfoDiferencaPorcentagem"))

    return usuarioTr
}

//Cria UMA coluna (um td)
function montaTd(dado, classe) {
    let usuarioTd = document.createElement("td")
    usuarioTd.classList.add(classe)
    usuarioTd.textContent = dado

    return usuarioTd
}